//
//  ApiConstants.swift
//  Data
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

class ApiConstants {
    let getMethod = "Get"
    let contentType = "Content-Type"
    let config = "application/json; charset=utf8"
}
