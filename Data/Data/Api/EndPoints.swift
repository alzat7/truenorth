//
//  EndPoints.swift
//  Data
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

class EndPoints {
    let baseUrl = "https://www.reddit.com/"
    let top = "top.json?limit=%d"
    let moreTop = "top.json?limit=%d&after=%@"
}
