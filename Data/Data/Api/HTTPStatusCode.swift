//
//  HTTPStatusCode.swift
//  Data
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

enum HTTPStatusCode: Int, Error {
    case success = 200
    case badRequest = 400
    case notFound = 404
    case internalServerError = 500
}

extension HTTPURLResponse {
    var status: HTTPStatusCode? {
        return HTTPStatusCode(rawValue: statusCode)
    }
}
