//
//  DetailInteractor.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 12/01/22.
//  
//

import Foundation

final public class DetailInteractor: DetailInteractorType {
    
    weak public var ownPresenter: DetailInteractorCallBackType?    
    public var ownRepository: EntryRepositoryType?
    
    public init () { }

    public func fetchDetail(id: String) {
        ownRepository?.fetchDetail(entryId: id)
    }
}

extension DetailInteractor: DetailRepositoryCallBackType {
    public func fetchDetailCallBack(entry: APIDetail) {
        ownPresenter?.fetchDetailCallBack(entry: entry)
    }
    
    public func errorCallBack(error: Error) {
        ownPresenter?.errorCallBack(error: error)
    }
}
