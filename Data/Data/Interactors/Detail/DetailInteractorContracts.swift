//
//  DetailInteractorContracts.swift
//  Data
//
//  Created by Alejandro  Alzate on 12/01/22.
//

import Foundation

public protocol DetailInteractorType: AnyObject {
    // PRESENTER -> INTERACTOR
    var ownPresenter: DetailInteractorCallBackType? { get set }
    var ownRepository: EntryRepositoryType? { get set }
    
    func fetchDetail(id: String)
}

public protocol DetailInteractorCallBackType: AnyObject {
    // INTERACTOR -> PRESENTER
    func fetchDetailCallBack(entry: APIDetail)
    func errorCallBack(error: Error)
}
