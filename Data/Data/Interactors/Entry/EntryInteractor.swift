//
//  EntryInteractor.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//  
//

import Foundation

final public class EntryInteractor: EntryInteractorType {
    
    weak public var ownPresenter: EntryInteractorCallBackType?
    public var ownRepository: EntryRepositoryType?
    
    public init() { }
    
    public func fetchEntries(limit: Int) {
        ownRepository?.fetchEntries(limit: limit)
    }
    
    public func fetchMoreEntries(limit: Int) {
        ownRepository?.fetchMoreEntries(limit: limit)
    }
}

extension EntryInteractor: EntryRepositoryCallBackType {
    
    public func fetchEntriesCallBack(entries: [APIEntry]) {
        ownPresenter?.fetchEntriesCallBack(entries: entries)
    }
    
    public func fetchMoreEntriesCallBack(entries: [APIEntry]) {
        ownPresenter?.fetchMoreEntriesCallBack(entries: entries)
    }
    
    public func errorCallBack(error: Error) {
        ownPresenter?.errorCallBack(error: error)
    }
}
