//
//  EntryInteractorContracts.swift
//  Data
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

public protocol EntryInteractorType: AnyObject {
    // PRESENTER -> INTERACTOR
    var ownPresenter: EntryInteractorCallBackType? { get set }
    var ownRepository: EntryRepositoryType? { get set }
    
    func fetchEntries(limit: Int)
    func fetchMoreEntries(limit: Int)
}

public protocol EntryInteractorCallBackType: AnyObject {
    // INTERACTOR -> PRESENTER
    func fetchEntriesCallBack(entries: [APIEntry])
    func fetchMoreEntriesCallBack(entries: [APIEntry])
    func errorCallBack(error: Error)
}
