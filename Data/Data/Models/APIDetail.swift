//
//  APIDetail.swift
//  Data
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation

public struct APIDetailResponse: Codable {
    public let kind: String
    public let data: APIDetails
}

public struct APIDetails: Codable {
    public let children: [APIDetail]
}

public struct APIDetail: Codable {
    public let kind: String
    public let data: APIDetailData
}

public struct APIDetailData: Codable {
    public let title: String?
    public let picture: String?
    public let pictureWidth: Int?
    public let pictureHeight: Int?
    
    enum CodingKeys: String, CodingKey {
        case picture = "thumbnail"
        case pictureWidth = "thumbnail_width"
        case pictureHeight = "thumbnail_height"
        case title
    }
}
