//
//  APIEntry.swift
//  Data
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

struct APIEntryResponse: Codable {
    let data: APIEntries
}

struct APIEntries: Codable {
    let after: String
    let entries: [APIEntry]
    
    enum CodingKeys: String, CodingKey {
        case entries = "children"
        case after
    }
}

public struct APIEntry: Codable {
    public let kind: String
    public let data: APIEntryData
}

public struct APIEntryData: Codable {
    public let id: String?
    public let title: String
    public let author: String
    public let date: Int
    public let thumbnail: String?
    public let thumbnailHeight: Int?
    public let thumbnailWidth: Int?
    public let numberOfComments: Int
    public let unreadStatus: Bool
    
    enum CodingKeys: String, CodingKey {
        case date = "created"
        case numberOfComments = "num_comments"
        case unreadStatus = "clicked"
        case thumbnailHeight = "thumbnail_height"
        case thumbnailWidth = "thumbnail_width"
        case id, title, author, thumbnail
    }
}
