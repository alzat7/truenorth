//
//  APIClient.swift
//  Data
//
//  Created by Alejandro  Alzate on 17/01/22.
//

import Foundation

final class APIClient {
    
    let session = URLSession.shared
    
    func request<T: Codable>(_ request: URLRequest?, completion: @escaping (Result<T, Error>) -> Void) {
        
        let completion = completion
        
        guard let request = request else {
            completion(.failure(HTTPStatusCode.notFound))
            return
        }
        
        session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil, let response = response as? HTTPURLResponse else {
                completion(.failure(error!))
                return
            }
            
            if response.status == HTTPStatusCode.success {
                do {
                    let decodedResponse = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(decodedResponse))
                } catch {
                    completion(.failure(error))
                }
            } else {
                completion(.failure(response.status ?? HTTPStatusCode.internalServerError))
            }
        }.resume()
    }
}
