//
//  BaseRepository.swift
//  Data
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

public class BaseRepository {
    var endPoint = EndPoints()
    var apiConstants = ApiConstants()
    let session = URLSession.shared
    var request: URLRequest?
    
    func createRequest(url: String){
        guard let url = URL(string: url) else {
            return
        }
        request = URLRequest(url: url)
        request?.httpMethod = apiConstants.getMethod
        request?.setValue(apiConstants.config, forHTTPHeaderField: apiConstants.contentType)
    }
}
