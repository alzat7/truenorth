//
//  EntryRepository.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//  
//

import Foundation

final public class EntryRepository: BaseRepository, EntryRepositoryType {
    
    public var ownEntryInteractor: EntryRepositoryCallBackType?
    public var ownDetailInteractor: DetailRepositoryCallBackType?
    private var afterId: String = ""
    
    public override init() { }
    
    public func fetchEntries(limit: Int) {
        
        let url = endPoint.baseUrl + String(format: endPoint.top, limit)
        createRequest(url: url)
        
        APIClient().request(request) { (result: Result<APIEntryResponse, Error>) in
            switch result {
            case .success(let entries):
                self.afterId = entries.data.after
                self.ownEntryInteractor?.fetchEntriesCallBack(entries: entries.data.entries)
                return
            case .failure(let error):
                self.ownEntryInteractor?.errorCallBack(error: error)
            }
        }
    }
    
    public func fetchMoreEntries(limit: Int) {
        let url = endPoint.baseUrl + String(format: endPoint.moreTop, limit, afterId)
        createRequest(url: url)
        
        APIClient().request(request) { (result: Result<APIEntryResponse, Error>) in
            switch result {
            case .success(let entries):
                self.afterId = entries.data.after
                self.ownEntryInteractor?.fetchMoreEntriesCallBack(entries: entries.data.entries)
            case .failure(let error):
                self.ownEntryInteractor?.errorCallBack(error: error)
            }
        }
    }
    
    public func fetchDetail(entryId: String) {
        let url = "\(endPoint.baseUrl)\(entryId).json"
        createRequest(url: url)
        
        APIClient().request(request) { (result: Result<[APIDetailResponse], Error>) in
            switch result {
            case .success(let details):
                if let detailResponse = details.first, let entry = detailResponse.data.children.first {
                    self.ownDetailInteractor?.fetchDetailCallBack(entry: entry)
                } else {
                    self.ownEntryInteractor?.errorCallBack(error: HTTPStatusCode.notFound)
                }
            case .failure(let error):
                self.ownDetailInteractor?.errorCallBack(error: error)
            }
        }
    }
}
