//
//  EntryRepositoryContracts.swift
//  Data
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

public protocol EntryRepositoryType: AnyObject {
    // INTERACTOR -> REPOSITORY
    var ownEntryInteractor: EntryRepositoryCallBackType? { get set }
    var ownDetailInteractor: DetailRepositoryCallBackType? { get set }
    
    func fetchEntries(limit: Int)
    func fetchMoreEntries(limit: Int)
    func fetchDetail(entryId: String)
}

public protocol EntryRepositoryCallBackType: AnyObject {
    // REPOSITORY -> INTERACTOR
    func fetchEntriesCallBack(entries: [APIEntry])
    func fetchMoreEntriesCallBack(entries: [APIEntry])
    func errorCallBack(error: Error)
}

public protocol DetailRepositoryCallBackType: AnyObject {
    // REPOSITORY -> INTERACTOR
    func fetchDetailCallBack(entry: APIDetail)
    func errorCallBack(error: Error)
}
