//
//  BaseContracts.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import Foundation

protocol BaseViewControllerType {
    func showAlert(title: String, description: String, positiveText: String?, cancelText: String, completionHandler: @escaping () -> Void)
    func showError(message: String)
    func showProgress()
    func hideProgress()
}
