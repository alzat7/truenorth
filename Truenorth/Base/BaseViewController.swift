//
//  BaseViewController.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import UIKit
import ProgressHUD

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ProgressHUD.animationType = .circleStrokeSpin
    }
}

extension BaseViewController: BaseViewControllerType {
    
    func showProgress() {
        DispatchQueue.main.async {
            ProgressHUD.show()
        }
    }
    
    func hideProgress() {
        DispatchQueue.main.async {
            ProgressHUD.dismiss()
        }
    }
    
    func showAlert(title: String, description: String, positiveText: String?, cancelText: String = Strings.Common.Button.cancel, completionHandler: @escaping () -> Void) {
        let alert = UIAlertController(
            title: title,
            message: description,
            preferredStyle: .alert)
        
        if let positiveText = positiveText {
            alert.addAction(UIAlertAction(title: positiveText, style: .default, handler: { _ in
                completionHandler()
            }))
        }
        
        alert.addAction(UIAlertAction(title: cancelText, style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    func showError(message: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(
                title: Strings.Alert.CommonError.title,
                message: message,
                preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: Strings.Common.Button.ok, style: .default, handler: nil))            
        }
    }
}
