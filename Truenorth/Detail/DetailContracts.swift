//
//  DetailContracts.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 12/01/22.
//  
//

import Data
import UIKit

protocol DetailViewControllerType: AnyObject {
    // PRESENTER -> VIEW
    var ownPresenter: DetailPresenterType? { get set }
    
    func showDetail(_ entry: DetailModel)
    func showError(error: Error)
}

protocol DetailRouterType: AnyObject {
    // PRESENTER -> ROUTER
    static func createDetailModule(id: String) -> UIViewController
}

protocol DetailPresenterType: AnyObject {
    // VIEW -> PRESENTER
    var ownViewController: DetailViewControllerType? { get set }
    var ownInteractor: DetailInteractorType? { get set }
    var ownRouter: DetailRouterType? { get set }
    var id: String? { get set }
    
    func viewDidLoad()
}
