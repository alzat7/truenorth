//
//  DetailPresenter.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 12/01/22.
//  
//

import Foundation
import Data

final class DetailPresenter  {
    
    // MARK: Properties
    weak var ownViewController: DetailViewControllerType?
    var ownInteractor: DetailInteractorType?
    var ownRouter: DetailRouterType?
    var id: String?
    
    private func unwrapData(entry: APIDetail) -> DetailModel {
        return DetailModel(
            title: entry.data.title ?? "",
            picture: entry.data.picture ?? "",
            pictureWidth: entry.data.pictureWidth ?? 140,
            pictureHeight: entry.data.pictureHeight ?? 140)
    }
}

extension DetailPresenter: DetailPresenterType {
    func viewDidLoad() {
        guard let id = id else {
            return
        }
        ownInteractor?.fetchDetail(id: id)
    }
}

extension DetailPresenter: DetailInteractorCallBackType {
    func fetchDetailCallBack(entry: APIDetail) {
        let unwrappedEntry = unwrapData(entry: entry)
        ownViewController?.showDetail(unwrappedEntry)
    }
    
    func errorCallBack(error: Error) {
        ownViewController?.showError(error: error)
    }
}
