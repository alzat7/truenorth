//
//  DetailRouter.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 12/01/22.
//  
//

import Foundation
import UIKit
import Data

final class DetailRouter: DetailRouterType {

    class func createDetailModule(id: String) -> UIViewController {
        let viewController = DetailViewController()
        let presenter: DetailPresenterType & DetailInteractorCallBackType = DetailPresenter()
        let interactor: DetailInteractorType & DetailRepositoryCallBackType = DetailInteractor()
        let repository: EntryRepositoryType = EntryRepository()
        let router: DetailRouterType = DetailRouter()
        
        viewController.ownPresenter = presenter
        presenter.ownViewController = viewController
        presenter.ownInteractor = interactor
        presenter.ownRouter = router
        presenter.id = id
        interactor.ownPresenter = presenter
        interactor.ownRepository = repository
        repository.ownDetailInteractor = interactor
        
        return viewController
    }
}
