//
//  DetailViewController.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 12/01/22.
//  
//

import Foundation
import UIKit

final class DetailViewController: BaseViewController {

    // MARK: Properties
    var ownPresenter: DetailPresenterType?
    var picture: String?
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var pictureImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        image.layer.cornerRadius = 10
        image.isUserInteractionEnabled = true
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        showProgress()
        ownPresenter?.viewDidLoad()
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        pictureImageView.addGestureRecognizer(gesture)
        
        view.backgroundColor = .white
        view.addSubview(titleLabel)
        view.addSubview(pictureImageView)
        
        setTitleLabelConstraints()
        setPictureImageViewConstraints()
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        showAlert(
            title: Strings.Alert.DownloadPicture.title,
            description: Strings.Alert.DownloadPicture.description,
            positiveText: Strings.Common.Button.yes) {
            
            if let picture = self.pictureImageView.image {
                DispatchQueue.main.async {
                    UIImageWriteToSavedPhotosAlbum(picture, self, #selector(self.saveCompleted(_:didFinishSavingWithError:contextInfo:)), nil)
                }
            }
        }
    }
    
    @objc func saveCompleted(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        self.showAlert(
            title: Strings.Alert.DownloadSuccessfully.title,
            description: Strings.Alert.DownloadSuccessfully.description,
            positiveText: nil,
            cancelText: Strings.Common.Button.ok) {
        }
    }
    
    private func calculateImageHeight (sourceImage: DetailModel, scaledToWidth: CGFloat) -> CGFloat {
        let oldWidth = CGFloat( sourceImage.pictureWidth)
        let scaleFactor = scaledToWidth / oldWidth
        let newHeight = CGFloat(sourceImage.pictureHeight) * scaleFactor
        return newHeight
    }
    
    private func setTitleLabelConstraints() {
        let titleLabelConstraints = [
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            titleLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            titleLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20)
        ]
        
        NSLayoutConstraint.activate(titleLabelConstraints)
    }
    
    private func setPictureImageViewConstraints() {
        let pictureImageViewConstraints = [
            pictureImageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30),
            pictureImageView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            pictureImageView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            pictureImageView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: -20),
            pictureImageView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100)
        ]
        
        NSLayoutConstraint.activate(pictureImageViewConstraints)
    }
}

extension DetailViewController: DetailViewControllerType {
    func showDetail(_ entry: DetailModel) {
        DispatchQueue.main.async {
            self.titleLabel.text = entry.title
            self.pictureImageView.downloaded(from: entry.picture)
            let pictureHeight = self.calculateImageHeight(
                sourceImage: entry,
                scaledToWidth: self.view.frame.size.width - 40)
            self.pictureImageView.heightAnchor.constraint(equalToConstant: pictureHeight).isActive = true
            self.hideProgress()
        }
    }
    
    func showError(error: Error) {
        showError(message: error.localizedDescription)
    }
}
