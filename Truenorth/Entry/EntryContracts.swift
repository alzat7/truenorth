//
//  EntryContracts.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//  
//

import Foundation
import UIKit
import Data

protocol EntryViewControllerType: AnyObject {
    // PRESENTER -> VIEW
    var ownPresenter: EntryPresenterType? { get set }
    
    func reloadData(_ entries: [EntryModel])
    func reloadMoreData(_ entries: [EntryModel])
    func showError(error: Error)
}

protocol EntryRouterType: AnyObject {
    // PRESENTER -> ROUTER
    static func createEntryModule() -> UIViewController
    
    func navigateToDetail(from view: EntryViewControllerType, with id: String)
}

protocol EntryPresenterType: AnyObject {
    // VIEW -> PRESENTER
    var ownViewController: EntryViewControllerType? { get set }
    var ownInteractor: EntryInteractorType? { get set }
    var ownRouter: EntryRouterType? { get set }
    
    func viewDidLoad()
    func itemDidSelected(entry: EntryModel)
    func loadMoreEntries()
}
