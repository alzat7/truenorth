//
//  EntryPresenter.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//  
//

import Foundation
import Data

final class EntryPresenter {
    
    weak var ownViewController: EntryViewControllerType?
    var ownInteractor: EntryInteractorType?
    var ownRouter: EntryRouterType?
    
    private func unwrapData(entries: [APIEntry]) -> [EntryModel] {
        return entries.map { entry in
            EntryModel(
                id: entry.data.id,
                title: entry.data.title,
                author: entry.data.author,
                date: entry.data.date,
                thumbnail: entry.data.thumbnail,
                thumbnailHeight: entry.data.thumbnailHeight,
                thumbnailWidth: entry.data.thumbnailWidth,
                numberOfComments: entry.data.numberOfComments,
                unreadStatus: entry.data.unreadStatus)
        }
    }
}

extension EntryPresenter: EntryPresenterType {
    
    func viewDidLoad() {
        ownInteractor?.fetchEntries(limit: 10)
    }
    
    func itemDidSelected(entry: EntryModel) {
        guard let viewController = ownViewController, let id = entry.id else {
            return
        }
        ownRouter?.navigateToDetail(from: viewController, with: id)
    }
    
    func loadMoreEntries() {
        ownInteractor?.fetchMoreEntries(limit: 10)
    }
}

extension EntryPresenter: EntryInteractorCallBackType {
    
    func fetchEntriesCallBack(entries: [APIEntry]) {
        let unwrappedEntries = unwrapData(entries: entries)
        ownViewController?.reloadData(unwrappedEntries)
    }
    
    func fetchMoreEntriesCallBack(entries: [APIEntry]) {
        let unwrappedEntries = unwrapData(entries: entries)
        ownViewController?.reloadMoreData(unwrappedEntries)
    }
    
    func errorCallBack(error: Error) {
        ownViewController?.showError(error: error)
    }
}
