//
//  EntryRouter.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//  
//

import Foundation
import UIKit
import Data

final class EntryRouter: EntryRouterType {

    class func createEntryModule() -> UIViewController {
        let viewController = EntryViewController()
        let presenter: EntryPresenterType & EntryInteractorCallBackType = EntryPresenter()
        let interactor: EntryInteractorType & EntryRepositoryCallBackType = EntryInteractor()
        let repository: EntryRepositoryType = EntryRepository()
        let router: EntryRouterType = EntryRouter()
        
        viewController.ownPresenter = presenter
        presenter.ownViewController = viewController
        presenter.ownRouter = router
        presenter.ownInteractor = interactor
        interactor.ownPresenter = presenter
        interactor.ownRepository = repository
        repository.ownEntryInteractor = interactor
        
        return viewController
    }
    
    func navigateToDetail(from view: EntryViewControllerType, with id: String) {
        let newDetailViewController = DetailRouter.createDetailModule(id: id)
        
        if let detailViewController = view as? UIViewController {
            detailViewController.navigationController?.pushViewController(newDetailViewController, animated: true)
        }
    }
}
