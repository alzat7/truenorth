//
//  EntryViewCell.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import UIKit
import SwipeCellKit

final class EntryViewCell: SwipeCollectionViewCell {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.setContentHuggingPriority(.defaultHigh, for: .vertical)
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var authorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var thumbnailImageView: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .systemGray4
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        image.layer.cornerRadius = 10
        image.setContentHuggingPriority(.defaultLow, for: .vertical)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    private lazy var numberOfCommentsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var unreadStatus: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(authorLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(thumbnailImageView)
        contentView.addSubview(numberOfCommentsLabel)
        
        setTitleLabelConstraints()
        setAuthorLabelConstraints()
        setDateLabelConstraints()
        setThumbnailImageViewConstraints()
        setNumberOfCommentsLabelConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setTitleLabelConstraints() {
        let titleLabelConstraints = [
            titleLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 5),
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10)
        ]
        
        NSLayoutConstraint.activate(titleLabelConstraints)
    }
    
    private func setAuthorLabelConstraints() {
        let authorLabelConstraints = [
            authorLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            authorLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
        ]
        
        NSLayoutConstraint.activate(authorLabelConstraints)
    }
    
    private func setDateLabelConstraints() {
        let dateLabelConstraints = [
            dateLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            dateLabel.leadingAnchor.constraint(greaterThanOrEqualTo: authorLabel.trailingAnchor, constant: 10),
            dateLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10)
        ]
        
        NSLayoutConstraint.activate(dateLabelConstraints)
    }
    
    private func setThumbnailImageViewConstraints() {
        let thumbnailImageViewConstraints = [
            thumbnailImageView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10),
            thumbnailImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            thumbnailImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10)
        ]
        
        NSLayoutConstraint.activate(thumbnailImageViewConstraints)
    }
    
    private func setNumberOfCommentsLabelConstraints() {
        let numberOfCommentsLabelConstrains = [
            numberOfCommentsLabel.topAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor, constant: 10),
            numberOfCommentsLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            numberOfCommentsLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            numberOfCommentsLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
        ]
        
        NSLayoutConstraint.activate(numberOfCommentsLabelConstrains)
    }
    
    func setTitle(_ title: String) {
        titleLabel.text = title
    }
    
    func setAuthor(_ author: String) {
        authorLabel.text = author
    }
    
    func setDate(_ date: Int) {
        dateLabel.text = String(format: Strings.Cell.Entry.timeAgo, date)
    }
    
    func setImage(url image: String?) {
        if let image = image {
            thumbnailImageView.downloaded(from: image)
        } else {
            thumbnailImageView.isHidden = true
        }
    }
    
    func setGestureToImage(gesture: UIGestureRecognizer) {
        thumbnailImageView.addGestureRecognizer(gesture)
    }
    
    func setNumberOfComments(_ numberOfComments: Int) {
        numberOfCommentsLabel.text = String(format: Strings.Cell.Entry.comments, numberOfComments)
    }
}
