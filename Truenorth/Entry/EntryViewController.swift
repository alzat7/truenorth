//
//  EntryViewController.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//  
//

import Foundation
import UIKit
import SwipeCellKit

private let reuseIdentifier = "EntryViewCell"

final class EntryViewController: BaseViewController {

    // MARK: Properties
    private var entries = [EntryModel]()
    var ownPresenter: EntryPresenterType?
    private let refreshControl = UIRefreshControl()
    private var isOnFetchingProcess = false
    
    private lazy var flowLayout: UICollectionViewFlowLayout = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        return flowLayout
    }()
    
    private lazy var collectionView: UICollectionView = {
        var view = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var thumbnailImageView: UIImageView = {
        let image = UIImageView()
        return image
    }()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ownPresenter?.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        collectionView.refreshControl = refreshControl
        collectionView.backgroundColor = .white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(EntryViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        view.addSubview(collectionView)
        setCollectionViewConstraints()
    }
    
    @objc private func refreshData(_ sender: Any) {
        ownPresenter?.viewDidLoad()
    }
    
    private func loadMoreEntries() {
        if !isOnFetchingProcess {
            isOnFetchingProcess = true
            ownPresenter?.loadMoreEntries()
        }
    }
    
    private func calculateImageHeight (sourceImage: EntryModel, scaledToWidth: CGFloat) -> CGFloat {
        let imageWidth = CGFloat(sourceImage.thumbnailWidth ?? 140)
        let scaleFactor = scaledToWidth / imageWidth
        let newHeight = CGFloat(sourceImage.thumbnailHeight ?? 140) * scaleFactor
        return newHeight
    }
    
    private func requiredHeight(text: String, cellWidth: CGFloat) -> CGFloat {
        let font = UIFont.boldSystemFont(ofSize: 20)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: cellWidth, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        return label.frame.height
    }
    
    private func setCollectionViewConstraints() {
        let collectionViewConstraints = [
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(collectionViewConstraints)
    }
}

extension EntryViewController: EntryViewControllerType {
    func reloadData(_ entries: [EntryModel]) {
        DispatchQueue.main.async {
            self.entries.removeAll()
            self.entries.append(contentsOf: entries)
            self.collectionView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    func reloadMoreData(_ entries: [EntryModel]) {
        DispatchQueue.main.async {
            self.entries.append(contentsOf: entries)
            self.collectionView.reloadData()
            self.isOnFetchingProcess = false
        }
    }
    
    func showError(error: Error) {
        showError(message: error.localizedDescription)
    }
}

extension EntryViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return entries.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let currentItem = entries[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! EntryViewCell
        let currentDate = Date()
        let cirrentDateInt = Int(currentDate.timeIntervalSince1970)
        let difference = (cirrentDateInt - currentItem.date) / 3600
        cell.delegate = self
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor.systemGray4.cgColor
        cell.setTitle(currentItem.title)
        cell.setAuthor(currentItem.author)
        cell.setDate(difference)
        cell.setImage(url: currentItem.thumbnail)
        cell.setNumberOfComments(currentItem.numberOfComments)
        return cell
    }
}

extension EntryViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentItem = entries[indexPath.row]
        ownPresenter?.itemDidSelected(entry: currentItem)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if offsetY > contentHeight - scrollView.frame.size.height {
            loadMoreEntries()
        }
    }
}

extension EntryViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let imageHeight = calculateImageHeight(
            sourceImage: entries[indexPath.row],
            scaledToWidth: collectionView.frame.size.width)
        
        let textHeight = requiredHeight(
            text: entries[indexPath.row].title,
            cellWidth: collectionView.frame.size.width)
        
        return CGSize(width: collectionView.frame.size.width - 20, height: (imageHeight + textHeight + 10))
    }
}

extension EntryViewController: SwipeCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else {
            return nil
        }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Remove") { action, indexPath in
            self.entries.remove(at: indexPath.row)
            action.fulfill(with: .delete)
        }
        
        return [deleteAction]
    }
    
    func collectionView(_ collectionView: UICollectionView, editActionsOptionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = .destructive
        options.transitionStyle = .reveal
        return options
    }
}
