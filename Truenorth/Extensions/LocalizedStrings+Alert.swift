//
//  LocalizedStrings+Alert.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 11/01/22.
//

import Foundation

extension Strings {
    enum Alert {
        enum DownloadPicture {
            static let title = "Alert.DownloadPicture.Title".localized()
            static let description = "Alert.DownloadPicture.Description".localized()
        }
        
        enum DownloadSuccessfully {
            static let title = "Alert.DownloadSuccessfully.Title".localized()
            static let description = "Alert.DownloadSuccessfully.Description".localized()
        }
        
        enum CommonError {
            static let title = "Alert.CommonError.Title".localized()
        }
    }
}
