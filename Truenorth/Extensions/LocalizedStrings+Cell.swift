//
//  LocalizedStrings+Cell.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation

extension Strings {
    enum Cell {
        enum Entry {
            static let comments = "Cell.Entry.Comments".localized()
            static let timeAgo = "Cell.Entry.TimeAgo".localized()
        }
    }
}
