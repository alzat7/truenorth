//
//  LocalizedStrings+Common.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 11/01/22.
//

import Foundation

extension Strings {
    enum Common {
        enum Button {
            static let yes = "Common.Button.Yes".localized()
            static let ok = "Common.Button.Ok".localized()
            static let cancel = "Common.Button.Cancel".localized()
        }
    }
}
