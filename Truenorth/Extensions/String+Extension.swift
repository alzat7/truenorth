//
//  String+Extension.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 11/01/22.
//

import Foundation

extension String {
    func localized(comment: String = "") -> String {
        return NSLocalizedString(self, comment: comment)
    }
}
