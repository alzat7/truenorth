//
//  DetailModel.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation

struct DetailModel {
    let title: String
    let picture: String
    let pictureWidth: Int
    let pictureHeight: Int    
}
