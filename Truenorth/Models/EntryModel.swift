//
//  EntryModel.swift
//  Truenorth
//
//  Created by Alejandro  Alzate on 10/01/22.
//

import UIKit

struct EntryModel {
    let id: String?
    let title: String
    let author: String
    let date: Int
    let thumbnail: String?
    let thumbnailHeight: Int?
    let thumbnailWidth: Int?
    let numberOfComments: Int
    let unreadStatus: Bool
}
