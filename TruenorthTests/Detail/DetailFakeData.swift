//
//  DetailFakeData.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation
@testable import Data

final class DetailFakeData {
    static func createFakeData() -> APIDetail {
        return APIDetail(
            kind: "Listing",
            data: APIDetailData(
                title: "Leaked Drone footage of shackled and blindfolded Uighur Muslims led from trains. Such a chilling footage.",
                picture: "https://b.thumbs.redditmedia.com/Nm23WaKwnPIC235k6WCgrCHfF6YwtQVg8Z7i00rZFeA.jpg",
                pictureWidth: 140,
                pictureHeight: 78))
    }
}
