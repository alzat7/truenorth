//
//  DetailInteractorStub.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation
@testable import Data

final class DetailInteractorStub: DetailInteractorType {
    var ownPresenter: DetailInteractorCallBackType?    
    var ownRepository: EntryRepositoryType?
    var returnSuccess: Bool = true
    
    func fetchDetail(id: String) {
        if returnSuccess {
            ownPresenter?.fetchDetailCallBack(entry: DetailFakeData.createFakeData())
        } else {
            ownPresenter?.errorCallBack(error: HTTPStatusCode.internalServerError)
        }
    }
}
