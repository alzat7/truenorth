//
//  DetailPresenterTest.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import XCTest
@testable import Truenorth
@testable import Data

final class DetailPresenterTest: XCTestCase {
    var viewControllerSpy: DetailViewControllerSpy!
    var interactorStub: DetailInteractorStub!
    var sut: (DetailPresenterType & DetailInteractorCallBackType)!
    
    override func setUp() {
        super.setUp()
        sut = DetailPresenter()
        viewControllerSpy = DetailViewControllerSpy()
        viewControllerSpy.ownPresenter = sut
        interactorStub = DetailInteractorStub()
        interactorStub.ownPresenter = sut
        sut.ownViewController = viewControllerSpy
        sut.ownInteractor = interactorStub
        sut.id = "s2zj1l"
    }
    
    override func tearDown() {
        viewControllerSpy = nil
        interactorStub = nil
        sut = nil
        super.tearDown()
    }
    
    func testViewDidLoad_WhenIsLoadingViewController_ThenReturnSuccess() {
        //Given
        let expectation = self.expectation(description: "present entries")
        var expectedModel: DetailModel?
        viewControllerSpy.showDetailCallBack = { detail in
            expectation.fulfill()
            expectedModel = detail
        }
        
        //When
        sut.viewDidLoad()
        
        //Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertNotNil(expectedModel)
    }
    
    func testViewDidLoad_WhenIsLoadingViewController_ThenConfirmValues() {
        //Given
        let expectation = self.expectation(description: "present entries")
        var actualModel = DetailModel(title: "", picture: "", pictureWidth: 1, pictureHeight: 1)
        let expectedModel = DetailFakeData.createFakeData()
        viewControllerSpy.showDetailCallBack = { detail in
            expectation.fulfill()
            actualModel = detail
        }
        
        //When
        sut.viewDidLoad()
        
        //Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertEqual(expectedModel.data.title, actualModel.title)
        XCTAssertEqual(expectedModel.data.picture, actualModel.picture)
        XCTAssertEqual(expectedModel.data.pictureWidth, actualModel.pictureWidth)
        XCTAssertEqual(expectedModel.data.pictureHeight, actualModel.pictureHeight)        
    }
    
    func testViewDidLoad_WhenIsLoadingViewController_ThenReturnError() {
        //Given
        let expectation = self.expectation(description: "show error")
        var actualError: Error?
        interactorStub.returnSuccess = false
        viewControllerSpy.showErrorCallBack = { error in
            expectation.fulfill()
            actualError = error
        }
        
        //When
        sut.viewDidLoad()
        
        //Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertNotNil(actualError)
    }
}
