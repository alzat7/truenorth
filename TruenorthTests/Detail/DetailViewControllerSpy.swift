//
//  DetailViewControllerSpy.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation
@testable import Truenorth

final class DetailViewControllerSpy: DetailViewControllerType {
    var ownPresenter: DetailPresenterType?
    var showDetailCallBack: ((DetailModel) -> Void)?
    var showErrorCallBack: ((Error) -> Void)?
    
    func showDetail(_ entry: DetailModel) {
        showDetailCallBack?(entry)
    }
    
    func showError(error: Error) {
        showErrorCallBack?(error)
    }
}
