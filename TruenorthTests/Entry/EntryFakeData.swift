//
//  EntryFakeData.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation
@testable import Data

final class EntryFakeData {
    static func createFakeData() -> [APIEntry] {
        return [
            APIEntry(
                kind: "Listing",
                data: APIEntryData(
                    id: "s2zj1l",
                    title: "Leaked Drone footage of shackled and blindfolded Uighur Muslims led from trains. Such a chilling footage.",
                    author: "mcmiller1111",
                    date: 1642080738,
                    thumbnail: "https://b.thumbs.redditmedia.com/Nm23WaKwnPIC235k6WCgrCHfF6YwtQVg8Z7i00rZFeA.jpg",
                    thumbnailHeight: 78,
                    thumbnailWidth: 140,
                    numberOfComments: 11627,
                    unreadStatus: false)),
            APIEntry(
                kind: "Listing",
                data: APIEntryData(
                    id: "s2zj1l",
                    title: "Leaked Drone footage of shackled and blindfolded Uighur Muslims led from trains. Such a chilling footage.",
                    author: "mcmiller1111",
                    date: 1642080738,
                    thumbnail: "https://b.thumbs.redditmedia.com/Nm23WaKwnPIC235k6WCgrCHfF6YwtQVg8Z7i00rZFeA.jpg",
                    thumbnailHeight: 78,
                    thumbnailWidth: 140,
                    numberOfComments: 11627,
                    unreadStatus: false)),
            APIEntry(
                kind: "Listing",
                data: APIEntryData(
                    id: "s2zj1l",
                    title: "Leaked Drone footage of shackled and blindfolded Uighur Muslims led from trains. Such a chilling footage.",
                    author: "mcmiller1111",
                    date: 1642080738,
                    thumbnail: "https://b.thumbs.redditmedia.com/Nm23WaKwnPIC235k6WCgrCHfF6YwtQVg8Z7i00rZFeA.jpg",
                    thumbnailHeight: 78,
                    thumbnailWidth: 140,
                    numberOfComments: 11627,
                    unreadStatus: false))
        ]
    }
}
