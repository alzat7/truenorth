//
//  EntryInteractorStub.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation
@testable import Data

final class EntryInteractorStub: EntryInteractorType {
    var ownPresenter: EntryInteractorCallBackType?
    var ownRepository: EntryRepositoryType?
    var returnSuccess: Bool = true
    
    func fetchEntries(limit: Int) {
        if returnSuccess {
            ownPresenter?.fetchEntriesCallBack(entries: EntryFakeData.createFakeData())
        } else {
            ownPresenter?.errorCallBack(error: HTTPStatusCode.internalServerError)
        }
    }
    
    func fetchMoreEntries(limit: Int) {
        if returnSuccess {
            ownPresenter?.fetchMoreEntriesCallBack(entries: EntryFakeData.createFakeData())
        } else {
            ownPresenter?.errorCallBack(error: HTTPStatusCode.internalServerError)
        }
    }
}
