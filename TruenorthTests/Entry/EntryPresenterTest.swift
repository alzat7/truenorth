//
//  EntryPresenterTest.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import XCTest
@testable import Truenorth
@testable import Data

final class EntryPresenterTest: XCTestCase {
    var viewControllerSpy: EntryViewControllerSpy!
    var interactorStub: EntryInteractorStub!
    var sut: (EntryPresenterType & EntryInteractorCallBackType)!
    
    override func setUp() {
        super.setUp()
        sut = EntryPresenter()
        viewControllerSpy = EntryViewControllerSpy()
        viewControllerSpy.ownPresenter = sut
        interactorStub = EntryInteractorStub()
        interactorStub.ownPresenter = sut
        sut.ownViewController = viewControllerSpy
        sut.ownInteractor = interactorStub
    }
    
    override func tearDown() {
        viewControllerSpy = nil
        interactorStub = nil
        sut = nil
        super.tearDown()
    }
    
    func testViewDidLoad_WhenIsLoadingViewController_ThenReturnSuccess() {
        //Given
        let expectation = self.expectation(description: "present entries")
        let expectedCountValue = 3
        var actualCountValue = 0
        viewControllerSpy.reloadDataCallBack = { entries in
            expectation.fulfill()
            actualCountValue = entries.count
        }
        
        //When
        sut.viewDidLoad()
        
        //Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertEqual(expectedCountValue, actualCountValue)
    }
    
    func testViewDidLoad_WhenIsLoadingViewController_ThenReturnError() {
        //Given
        let expectation = self.expectation(description: "show error")
        var actualError: Error?
        interactorStub.returnSuccess = false
        viewControllerSpy.showErrorCallBack = { error in
            expectation.fulfill()
            actualError = error
        }
        
        //When
        sut.viewDidLoad()
        
        //Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertNotNil(actualError)
    }
    
    func testLoadMoreEntries_WhenScrollIsAtTheEnd_ThenLoadMoreInfo() {
        //Given
        let expectation = self.expectation(description: "load more entries")
        let expectedCountValue = 3
        var actualCountValue = 0
        viewControllerSpy.reloadMoreDataCallBack = { entries in
            expectation.fulfill()
            actualCountValue = entries.count
        }
        
        //When
        sut.loadMoreEntries()
        
        //Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertEqual(expectedCountValue, actualCountValue)
    }
    
    func testLoadMoreEntries_WhenScrollIsAtTheEnd_ThenReturnError() {
        //Given
        let expectation = self.expectation(description: "show error")
        var actualError: Error?
        interactorStub.returnSuccess = false
        viewControllerSpy.showErrorCallBack = { error in
            expectation.fulfill()
            actualError = error
        }
        
        //When
        sut.loadMoreEntries()
        
        //Then
        wait(for: [expectation], timeout: 1.0)
        XCTAssertNotNil(actualError)
    }
}
