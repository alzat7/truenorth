//
//  EntryViewControllerSpy.swift
//  TruenorthTests
//
//  Created by Alejandro  Alzate on 13/01/22.
//

import Foundation
@testable import Truenorth

final class EntryViewControllerSpy: EntryViewControllerType {
    var ownPresenter: EntryPresenterType?
    
    var reloadDataCallBack: (([EntryModel]) -> Void)?
    var reloadMoreDataCallBack: (([EntryModel]) -> Void)?
    var showErrorCallBack: ((Error) -> Void)?
    
    func reloadData(_ entries: [EntryModel]) {
        reloadDataCallBack?(entries)
    }
    
    func reloadMoreData(_ entries: [EntryModel]) {
        reloadMoreDataCallBack?(entries)
    }
    
    func showError(error: Error) {
        showErrorCallBack?(error)
    }
}
